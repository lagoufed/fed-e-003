const MayBe = require('folktale/maybe')

const toRMB = money => MayBe.fromNullable(money)
  .map(v => v.replace('$', ''))
  .map(parseFloat)
  .map(v => v / 7)
  .map(v => v.toFixed(2))
  .map(v => '¥' + v)
  .getOrElse('nothing')

console.log(toRMB(null))


/**
 * 一个加的方法
 * @param { 第一个参数 } a 
 * @param {*} b 
 */
function add (a, b) {

}





