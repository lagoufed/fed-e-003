function fn (a, b) {
  function inner () {
    console.log(a, b)
  }
  console.dir(inner)
  return inner
}
console.dir(fn)
const f = fn(1, 2)
f()