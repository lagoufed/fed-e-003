# Changelog

## [0.1.1] - 2020-08-09

- fix: skip binary files when rendering templates (#5)
- chore: update dependency ora to v5.0.0

## [0.1.0] - 2020-08-02

- chore: initial release

## [0.0.0] - 2020-07-14

- feat: initial commit

<!-- http://keepachangelog.com/ -->
