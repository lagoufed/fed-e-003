// 定义类型的文件，类型声明文件

// 这里的作用就是告诉 TS 如果 import 了 .vue 结尾的文件，你得到的何种类型的数据
declare module '*.vue' {
  import Vue from 'vue'
  // interface Foo {
  //   a: string
  // }
  // const f: Foo
  export default Vue
}

declare interface Dict<T> {
  [key: string]: T
}

// ESM 模块作用域
// export {}