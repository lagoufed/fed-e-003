import Vue from 'vue'
// 不知道这个文件是什么类型
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// // Foo 只是 TS 中的类型
// interface Foo {
//   a: string
// }

// class Bar {
// }

const dict: Dict<string> = {
  foo: '123',
  bar: '123'
}
